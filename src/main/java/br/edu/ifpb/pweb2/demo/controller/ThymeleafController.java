package br.edu.ifpb.pweb2.demo.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/horas")
public class ThymeleafController {

	@RequestMapping("/print")
	public String helloThymeleaf(Model model) {
		model.addAttribute("data", LocalDateTime.now());
		return "hello";
	}
	
	@RequestMapping("/fusos")
	public String gereFusos(Model m) {
		m.addAttribute("data", new Date());
		List<Integer> lista = new ArrayList<Integer>(); //Arrays.asList(new Integer[]{-3,-2,-1,0,1,2});
		m.addAttribute("lista", lista);
		return "hello";
	}
}
