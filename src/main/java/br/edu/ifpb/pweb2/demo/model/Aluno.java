package br.edu.ifpb.pweb2.demo.model;

public class Aluno {

	private String nome;
	private Long matricula;
	private String login;
	private String pais;
	private String linguagem;
	private String[] fluencia;
	
	

	public Aluno() {
		super();
	}

	public Aluno(String nome) {
		super();
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Long getMatricula() {
		return matricula;
	}

	public void setMatricula(Long matricula) {
		this.matricula = matricula;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getLinguagem() {
		return linguagem;
	}

	public void setLinguagem(String linguagem) {
		this.linguagem = linguagem;
	}

	public String[] getFluencia() {
		return fluencia;
	}

	public void setFluencia(String[] fluencia) {
		this.fluencia = fluencia;
	}

}
