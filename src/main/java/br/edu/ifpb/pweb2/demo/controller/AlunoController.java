package br.edu.ifpb.pweb2.demo.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.edu.ifpb.pweb2.demo.model.Aluno;
import br.edu.ifpb.pweb2.demo.repo.AlunoRepositorio;

@Controller
@RequestMapping("/aluno")
public class AlunoController {
	
	@RequestMapping("/showAlunoForm")
	public String showAlunoForm(Model model) {
		model.addAttribute("aluno", new Aluno());
		model.addAttribute("paisOptions", this.getPaisOption());
		model.addAttribute("linguagemOptions", this.getLinguagemOption());
		return "form-aluno";
	}
	
	@RequestMapping("/cadastreAluno")
	public String cadastreAluno(@ModelAttribute("aluno") Aluno  aluno) {
		System.out.println(aluno.getNome() + " cadastrado com sucesso!");
		return "aluno-cadastrado";
	}
	
	@RequestMapping("/showAlunoFormBinding")
	public String showAlunoFormBinding(Model model) {
		model.addAttribute("paisOptions", this.getPaisOption());
		model.addAttribute("linguagemOptions", this.getLinguagemOption());
		return "form-aluno-binding";
	}
	
	@RequestMapping("/cadastreAlunoBinding")
	public String cadastreAlunoBinding(Aluno  aluno) {
		System.out.println(aluno.getNome() + " cadastrado com sucesso!");
		return "aluno-cadastrado-binding";
	}
	
	@RequestMapping("/busque")
	public String busqueAluno(@RequestParam("id") Long id, Model model) {
		Aluno aluno = AlunoRepositorio.findById(id);
		model.addAttribute(aluno);
		return "aluno-cadastrado";
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public String busqueAlunoComPathParam(@PathVariable("id") Long id, Model model) {
		Aluno aluno = AlunoRepositorio.findById(id);
		model.addAttribute(aluno);
		return "aluno-cadastrado";
	}
	
	private Map<String, String> getPaisOption() {
		LinkedHashMap<String,String> options = new LinkedHashMap<String,String>();
		options.put("Brasil", "Brasil");
		options.put("Argentina", "Argentina");
		options.put("Colômbia", "Colômbia");
		options.put("Uruguai", "Uruguai");
		return options;
	}
	
	private Map<String, String> getLinguagemOption() {
		LinkedHashMap<String,String> options = new LinkedHashMap<String,String>();
		options.put("Java", "Java");
		options.put("Python", "Python");
		options.put("C++", "C++");
		return options;
	}
}
