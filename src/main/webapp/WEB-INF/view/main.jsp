<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Página Principal da Aplicação</title>
</head>
<body>

<h1>Spring MVC Demo  - Home Page</h1>

<a href="login/showLoginForm">Login</a><br/><br/>

<a href="aluno/showAlunoForm">Cadastro de alunos</a><br/><br/>

<a href="aluno/showAlunoFormBinding">Cadastro de alunos com binding</a><br/><br/>

<a href="cliente/showForm">Cadastro de clientes com validação</a><br/><br/>



</body>
</html>

