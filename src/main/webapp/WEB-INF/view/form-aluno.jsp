<%@ page language="java" 
	contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Cadastro de Aluno</title>
</head>
<body>
<form:form action="cadastreAluno" method="post" modelAttribute="aluno">
	Nome: <form:input path="nome"/><br/>
	
	Matr�cula: <form:input path="matricula" /><br/>
	
	Login: <form:input path="login" /><br/>
	
	Pa�s: <form:select path="pais">
		<form:options items="${paisOptions}"/>
	</form:select> <br/>
	
	Linguagem: <form:radiobuttons path="linguagem" items="${linguagemOptions}"  /><br/>
	
	Flu�ncia: <form:checkbox path="fluencia" value="Portugu�s" /> Portugu�s
	 <form:checkbox path="fluencia" value="Ingl�s" /> Ingl�s
	 <form:checkbox path="fluencia" value="Espanhol" /> Espanhol<br/>
	
<input type="submit" value="Salvar" />
</form:form>
</body>
</html>



