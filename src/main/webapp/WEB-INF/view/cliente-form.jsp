<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>

<head>
	<title>Formulário de Registro de clientes</title>
	
	<style type="text/css">
	.error {
		color:red
	}
	</style>

</head>

<body>
	<i>Preeencha os campos obrigatórios (*).</i>
	
	<br/><br/>

	<form:form action="processForm" modelAttribute="cliente">
		Nome:  <form:input path="nome"/>
		
		<br/><br/>
		
		CPF(*): <form:input path="cpf" />
		<form:errors path="cpf" cssClass="error" />
		
		<br/><br/>
		
		Idade: <form:input path="idade" />
		<form:errors path="idade" cssClass="error" />
		
		<br/><br/>
		
		CEP:  <form:input path="cep" />
		<form:errors path="cep" cssClass="error" />
		
		<br/><br/>
		
		Curso:  <form:input path="codigoCurso" />
		<form:errors path="codigoCurso" cssClass="error" />
		
		<br/><br/>
		
		<input type="submit" value="Enviar"/>
	
	</form:form>

</body>

</html>
